let element = document.getElementById("time");

let timerH4pp3ns;
let lSHours;
let lSMinutes;
let sTHours;
let sTMinutes;
let bTHours;
let bTMinutes;
let sTTier1;
let sTTier2;
let sTTier3;
let sTDonation;
let donationAmount;
let bitsAmount;
let sTBits;
let fontName;
let fontColor;
let fontSize;
let livePrefix;
let liveSincePrefix;
let breakPrefix;
let bTAdded;

window.addEventListener('onEventReceived', function (obj) {
  if (!obj.detail.event) {
    return;
  }
  
  if (typeof obj.detail.event.itemId !== "undefined") {
    obj.detail.listener = "redemption-latest"
  }

  const listener = obj.detail.listener.split("-")[0];
  const event = obj.detail.event;
    
  if (listener === 'subscriber') {
    let amount = 0;
    
    if(event.bulkGifted) {
      amount = event.amount;
    }
    else if(event.gifted) {
      if(!event.isCommunityGift) {
        amount = 1;
      }
    }
    else {
      amount = 1;
    }
    
    sub(amount, event.tier);
    
  } else if (listener === 'cheer') {
    bits(event.amount);
  } else if (listener === 'tip') {
    donation(event.amount);
  }
  else if(event.listener === "widget-button") {
    if(event.field === "timerH4pp3nsBreakLive" && event.value === "breakLive") {
      if(timerH4pp3ns.reset) {
        timerH4pp3ns.liveSinceTS = new Date().getTime() - lSHours * 3600 * 1000 - lSMinutes * 60 * 1000;
        
        timerH4pp3ns.liveTS = new Date().getTime() + sTHours * 3600 * 1000 + sTMinutes * 60 * 1000;
        
        timerH4pp3ns.breakTS = new Date().getTime() + bTHours * 3600 * 1000 + bTMinutes * 60 * 1000;
        timerH4pp3ns.breakLeft = timerH4pp3ns.breakTS - new Date().getTime();
        
        timerH4pp3ns.previousHour = lSHours;
        
        timerH4pp3ns.running = true;
        timerH4pp3ns.reset = false;
        save();
      }
      else if(timerH4pp3ns.running) {
        timerH4pp3ns.breakTS = timerH4pp3ns.breakLeft + new Date().getTime();
        
        timerH4pp3ns.running = false;
        save();
      }
      else {
        timerH4pp3ns.breakLeft = timerH4pp3ns.breakTS - new Date().getTime();
        timerH4pp3ns.running = true;
        timerH4pp3ns.reset = false;
        save();
      }
    }
    else if(event.field === "timerH4pp3nsReset" && event.value === "reset") {
      timerH4pp3ns.reset = true;
      save();
    }
  }
  else {

  }
});

let save = function() {
  SE_API.store.set('timerH4pp3ns', timerH4pp3ns);
}

let load = function() {
  SE_API.store.get('timerH4pp3ns').then(obj => {
    if(obj !== null) {
      timerH4pp3ns = obj;
      
      if(!timerH4pp3ns.running) {
        timerH4pp3ns.breakLeft = timerH4pp3ns.breakTS - new Date().getTime();
      }
      else {
        timerH4pp3ns.breakTS = timerH4pp3ns.breakLeft + new Date().getTime();
      }
    }
    else {
      timerH4pp3ns = new Object();

      timerH4pp3ns.liveSinceTS = new Date().getTime() - lSHours * 3600 * 1000 - lSMinutes * 60 * 1000;
      timerH4pp3ns.liveTS = new Date().getTime() + sTHours * 3600 * 1000 + sTMinutes * 60 * 1000;
      timerH4pp3ns.breakTS = new Date().getTime() + bTHours * 3600 * 1000 + bTMinutes * 60 * 1000;
      timerH4pp3ns.breakLeft = timerH4pp3ns.breakTS - new Date().getTime();
      timerH4pp3ns.previousHour = lSHours;
      
      timerH4pp3ns.running = true;
      timerH4pp3ns.reset = true;
      
      save();
    }
    
    autoRefresh();
  });
}

window.addEventListener('onWidgetLoad', function (obj) {
  const fieldData = obj.detail.fieldData;
  lSHours = fieldData["lSHours"];
  lSMinutes = fieldData["lSMinutes"];
  sTHours = fieldData["sTHours"];
  sTMinutes = fieldData["sTMinutes"];
  bTHours = fieldData["bTHours"];
  bTMinutes = fieldData["bTMinutes"];
  sTTier1 = fieldData["sTTier1"];
  sTTier2 = fieldData["sTTier2"];
  sTTier3 = fieldData["sTTier3"];
  sTDonation = fieldData["sTDonation"];
  donationAmount = fieldData["donationAmount"];
  bitsAmount = fieldData["bitsAmount"];
  sTBits = fieldData["sTBits"];
  fontName = fieldData["fontName"];
  fontColor = fieldData["fontColor"];
  fontSize = fieldData["fontSize"];
  liveSincePrefix = fieldData["liveSincePrefix"];
  livePrefix = fieldData["livePrefix"];
  breakPrefix = fieldData["breakPrefix"];
  bTAdded = fieldData["bTAdded"];
  
  load();
  
  document.getElementById("liveSincePrefix").innerHTML = liveSincePrefix + "&nbsp;";
  document.getElementById("livePrefix").innerHTML = livePrefix + "&nbsp;";
  document.getElementById("breakPrefix").innerHTML = breakPrefix + "&nbsp;";
  
  document.getElementById("style").href = "https://fonts.googleapis.com/css?family=" + fontName.replaceAll(" ","+");
  
  document.body.style.fontFamily = "'" + fontName + "', serif";
  document.body.style.color = fontColor;

  document.body.style.textShadow = "-2px 2px 2px #000, 1px 1px 2px #000, 1px -1px 0 #000, -1px -1px 0 #000";
  document.body.style.fontSize = fontSize + "px";
});

let sub = function(count, tier) {
  if(count > 0) {
    if(tier == "prime" || tier == 1000) {
      multiplier = sTTier1;
    }
    else if(tier == 2000) {
      multiplier = sTTier2;
    }
    else if(tier == 3000) {
      multiplier = sTTier3;
    }

    addLiveTime(count * multiplier);
  }
}

let bits = function(amount) {
  roundedAmount = Math.floor(amount / bitsAmount);
  addLiveTime(roundedAmount * sTBits);
}

let donation = function(amount) {
  roundedAmount = Math.floor(amount / donationAmount);
  addLiveTime(roundedAmount * sTDonation);
}

let addLiveTime = function(minutes) {
  timerH4pp3ns.liveTS += minutes * 60 * 1000;
  save();
}

let addBreakTime = function(minutes) {
  if(timerH4pp3ns.running) {
    timerH4pp3ns.breakLeft += minutes * 60 * 1000;
  }
  else {
    timerH4pp3ns.breakTS += minutes * 60 * 1000;
  }
  
  save();
}

drawLiveSince = function() {
  if(timerH4pp3ns.reset) {
    document.getElementById('liveSince').textContent = `${lSHours.toString().padStart(2, "0")}:${lSMinutes.toString().padStart(2, "0")}:00`;
  }
  else {
    let diff = Math.round((new Date().getTime() - timerH4pp3ns.liveSinceTS) / 1000);
    
    if(diff > 0) {
      let hours = Math.floor(diff / 60 / 60);
      diff = diff - hours * 60 * 60;
      let minutes = Math.floor(diff / 60);
      diff = diff - minutes * 60;
      let seconds = diff;

      document.getElementById('liveSince').textContent = `${hours.toString().padStart(2, "0")}:${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;

      if(hours > timerH4pp3ns.previousHour) {
        timerH4pp3ns.previousHour = hours;
        addBreakTime(bTAdded);
      }
    }
    else {
      document.getElementById('liveSince').textContent = "00:00:00";
    }
  }
}

drawLiveLeft = function() {
  if(timerH4pp3ns.reset) {
    document.getElementById('time').textContent = `${sTHours.toString().padStart(2, "0")}:${sTMinutes.toString().padStart(2, "0")}:00`;
  }
  else {
    let diff;
  
    diff = Math.round((timerH4pp3ns.liveTS - new Date().getTime()) / 1000);

    if(diff > 0) {
      let hours = Math.floor(diff / 60 / 60);
      diff = diff - hours * 60 * 60;
      let minutes = Math.floor(diff / 60);
      diff = diff - minutes * 60;
      let seconds = diff;

      document.getElementById('time').textContent = `${hours.toString().padStart(2, "0")}:${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;
    }
    else {
      document.getElementById('time').textContent = "00:00:00";
    }
  }
}

let drawBreakLeft = function() {
  if(timerH4pp3ns.reset) {
    document.getElementById('break').textContent = `${bTHours.toString().padStart(2, "0")}:${bTMinutes.toString().padStart(2, "0")}:00`;
  }
  else {
    let diff;
  
    if(!timerH4pp3ns.running) {
      diff = Math.round((timerH4pp3ns.breakTS - new Date().getTime()) / 1000);
    }
    else {
      diff = Math.round((timerH4pp3ns.breakLeft) / 1000);
    }

    if(diff > 0) {
      let hours = Math.floor(diff / 60 / 60);
      diff = diff - hours * 60 * 60;
      let minutes = Math.floor(diff / 60);
      diff = diff - minutes * 60;
      let seconds = diff;

      document.getElementById('break').textContent = `${hours.toString().padStart(2, "0")}:${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;
    }
    else {
      document.getElementById('break').textContent = "00:00:00";
    }
  }
}

let drawTimes = function() {
  drawLiveSince();
  drawLiveLeft();
  drawBreakLeft();
}

let autoRefresh = function() {
  drawTimes();
  setTimeout(autoRefresh, 1000);
}